**Provider:**

1. Cognito - to use the cognito provider, in the application that you use, add in the properties "jwt.cognito.issuer" with an indication of the cognito pool.
2. JWT default token - to use a default provider, you must add "jwt.issuer" in the properties using the application with an indication of what value will be in the "iss" in the token, and "jwt.secret", which is used to display or verify the token. The algorithm used is HMAC256 with "secret base64 encoded".

**Instalation:**

If you want to use this library in other repo, you should have nexus version 2 installed on port 8081 and created "test-releases" and then add dependencies to nexus. 
* docker-compose up -d
* Then add hosted repository in nexus at: http://IP:8081/nexus (default login: admin, password: admin123)

* If you use nexus locally you should not change anything. 
* If you use nexus in the cloud, you should change the nexus URL in the pom.xml file. 
* If you want to change the name of the repo in nexus you should make changes to the settings.xml and pom.xml files.

After correctly configuring, if you want to add changes to nexus, run the following commands:
* mvn compile --settings settings.xml
* mvn deploy --settings settings.xml

**Example use:**

1. Add dependency in pom.xml to the given library and to "spring-boot-starter-security"
2. Configure security, sample configuration:

```
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.authcommon")
public class WebSecurity extends WebSecurityConfigurerAdapter {
    private static final List<String> ALLOWED_ORIGIN_METHODS = Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS");

    private static final String[] AUTH_WHITELIST_SWAGGER = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/documentation",
            "/configuration/ui",
            "/configuration/security",
            "/webjars/**"
    };

    @Value("${jwt.cognito.issuer}")
    private String cognitoIssuer;

    @Value("${jwt.issuer}")
    private String issuer;

    private final JWTProvider cognitoJWTProvider;
    private final JWTProvider customJWTProvicer;
    private final JWSUtils jwsUtils;

    @Autowired
    public WebSecurity(
            @Qualifier("cognito-jwt-provider") final JWTProvider cognitoJWTProvider,
            @Qualifier("jwt-provider") final JWTProvider customJWTProvider,
            final JWSUtils jwsUtils) {

        this.cognitoJWTProvider = cognitoJWTProvider;
        this.customJWTProvicer = customJWTProvider;
        this.jwsUtils = jwsUtils;
    }

    @Override
    public void configure(org.springframework.security.config.annotation.web.builders.WebSecurity web) throws Exception {
        web.ignoring().antMatchers(AUTH_WHITELIST_SWAGGER);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        //shouldn't care about CSRF as not browser based
        httpSecurity.cors().and().csrf().disable()
                .antMatcher("/**")
                .addFilterAfter(new JWTDecoderFilter(cognitoJWTProvider, customJWTProvicer, cognitoIssuer, issuer, jwsUtils), BasicAuthenticationFilter.class);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin("*");
        configuration.addAllowedHeader("*");
        configuration.setAllowedMethods(ALLOWED_ORIGIN_METHODS);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
```
