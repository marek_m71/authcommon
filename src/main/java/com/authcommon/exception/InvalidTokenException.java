package com.authcommon.exception;

/**
 * Represents an invalid JWT token
 */
public class InvalidTokenException extends UnauthorizedException {

    /**
     * Exception with message
     *
     * @param message the message
     * @should create exception with message
     */
    public InvalidTokenException(final String message) {
        super(message);
    }
}
