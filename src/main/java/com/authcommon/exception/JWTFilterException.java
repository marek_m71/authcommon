package com.authcommon.exception;

/**
 * Represents a problem filtering the request with the provided JWT
 */
public class JWTFilterException extends UnauthorizedException {

    /**
     * Exception with message
     *
     * @param message the message
     * @should create exception with message
     */
    public JWTFilterException(String message) {
        super(message);
    }
}
