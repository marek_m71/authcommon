package com.authcommon.exception;

/**
 * Custom {@link RuntimeException} that handles 400 Bad Request HTTP error codes. Can be thrown inside the code so the
 * Spring exception handler can turn them into HTTP errors.
 */
public class BadRequestException extends RuntimeException {

    public BadRequestException(final String message) {
        super(message);
    }
}
