package com.authcommon.exception;

/**
 * Exception thrown when unable to create a JWS
 */
public class JWSException extends Exception {

    /**
     * Exception with message
     *
     * @param message the message
     * @should contain message
     */
    public JWSException(String message) {
        super(message);
    }

    /**
     * Exception with message and cause
     *
     * @param message the message
     * @param cause   the cause
     * @should contain message and cause
     */
    public JWSException(String message, Throwable cause) {
        super(message, cause);
    }
}
