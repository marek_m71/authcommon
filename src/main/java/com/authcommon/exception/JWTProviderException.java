package com.authcommon.exception;

/**
 * JWTProviderException for failure to generate a valid JWT token
 */
public class JWTProviderException extends RuntimeException {

    /**
     * Exception with message
     *
     * @param message the message
     * @should create exception with message
     */
    public JWTProviderException(final String message) {
        super(message);
    }

    /**
     * Exception with message and cause
     *
     * @param message the message
     * @param cause   the cause
     * @should create exception with message and cause
     */
    public JWTProviderException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
