package com.authcommon.exception;

/**
 * Custom {@link RuntimeException} that handles 401 Unauthorized HTTP error codes. Can be thrown inside the code so the
 * Spring exception handler can turn them into HTTP errors.
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException(final String message) {
        super(message);
    }
}
