package com.authcommon.util;

import com.authcommon.domain.Membership;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Convenience utility for getting the logged in membership from the security context
 */
public class MembershipRetriever {

    /**
     * Gets username from logged in membership
     *
     * @return the username
     * @should returns the username
     */
    public static String getUsername() {
        return ((Membership) (SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getId();
    }

    private MembershipRetriever() {
        //should not be instantiated
    }
}
