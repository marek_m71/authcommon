package com.authcommon.jws;

import com.authcommon.exception.JWSException;
import com.google.gson.Gson;
import com.nimbusds.jose.*;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Map;

/**
 * Utility service for creating JWS objects
 */
@Service
public class JWSUtils {

    private static final Logger LOG = LoggerFactory.getLogger(JWSUtils.class);
    private static final String JWS_HEADER_ALGORITHM_FIELD = "alg";

    private final Gson gson;

    @Autowired
    public JWSUtils(final Gson gson) {
        this.gson = gson;
    }

    /**
     * Creates a base 64 encoded serialised JWS object with the given payload and headers, signed by the given signer
     *
     * @param payload           the payload for the JWS. This can be any object that gson can translate into a json string
     * @param signer            the signer to use to sign the JWS
     * @param headerAlgorithm   the header algorithm - The {@link JWSAlgorithm} used to sign the JWS
     * @param additionalHeaders a map containing any additional JWS headers to add to the header object. Can be null or empty
     * @param <T>               a generic type for the payload. Gson will be used to convert this to a json object
     * @return a a base 64 encoded serialised JWS Object
     * @throws JWSException if signer cant sign the object
     * @should create correct object if all arguments valid
     * @should throw JWSException if header algorithm is null
     * @should throw JWSException if payload null
     * @should throw JWSException if signer null
     * @should throw JWSException if signer throws exception
     */
    public <T> String create(final T payload,
                             final JWSSigner signer,
                             final JWSAlgorithm headerAlgorithm,
                             final Map<String, String> additionalHeaders) throws JWSException {
        try {
            validate(payload, signer, headerAlgorithm);
            JWSObject jwsObject = new JWSObject(createHeader(headerAlgorithm, additionalHeaders), new Payload(gson.toJson(payload)));
            jwsObject.sign(signer);
            return jwsObject.serialize();
        } catch (ParseException | JOSEException e) {
            throw new JWSException("Unable to create JWS", e);
        }
    }

    /**
     * Gets a string claim from the given jws
     *
     * @param jws       the jws string
     * @param claimName the name of the claim to get
     * @return a string claim from the object, or null if not found
     * @throws JWSException if jws or claim name are null or empty, or if cannot parse the jws
     * @should get claim from jws
     * @should throw jws exception if jws is null
     * @should throw jws exception if jws is empty
     * @should throw jws exception if claimName is null
     * @should throw jws exception if claimName is empty
     * @should throw jws exception if jws cannot be parsed
     */
    public String getStringClaim(final String jws, final String claimName) throws JWSException {
        try {
            validate(jws, claimName);
            return JWSObject.parse(jws).getPayload().toJSONObject().getAsString(claimName);
        } catch (ParseException e) {
            LOG.error("Unable to parse JWT and thus unable to retrieve claim {}", claimName);
            throw new JWSException("Unable to parse jws", e);
        }
    }

    /**
     * Gets the Key ID stated in the header of the passed in JWS
     *
     * @param jws the JWS token
     * @return the key id from the header, if present
     * @throws JWSException if unable to validate or parse JWS
     * @should return key id if present in header
     * @should throw JWSException if unable to parse object
     * @should throw JWSException if jws is null
     * @should throw JWSException if jws is empty
     */
    public String getKeyId(final String jws) throws JWSException {
        try {
            validate(jws);
            return JWSObject.parse(jws).getHeader().getKeyID();
        } catch (ParseException e) {
            LOG.error("Unable to parse JWT and thus unable to retrieve Key Id");
            throw new JWSException("Unable to parse jws", e);
        }
    }

    /**
     * Validates that objects passed in are not null, and if instance of string, aren't empty or blank
     *
     * @throws JWSException if any objects passed in are null, and if instance of string, are empty or blank
     */
    private void validate(final Object... toVerify) throws JWSException {
        for (Object obj : toVerify) {
            if (obj == null || (obj instanceof String && StringUtils.isBlank((String) obj))) {
                throw new JWSException("One of the arguments was null");
            }
        }
    }

    /**
     * Creates a JOSE Header for the JWS
     *
     * @param headerAlgorithm   the header algorithm - The {@link JWSAlgorithm} used to sign the JWS
     * @param additionalHeaders a map containing any additional JWS headers to add to the header object. Can be null or empty
     * @return a valid {@link JWSHeader} object
     * @throws ParseException if
     */
    private JWSHeader createHeader(final JWSAlgorithm headerAlgorithm,
                                   final Map<String, String> additionalHeaders) throws ParseException {

        JSONObject header = new JSONObject();
        header.appendField(JWS_HEADER_ALGORITHM_FIELD, headerAlgorithm.getName());

        if (additionalHeaders != null) {
            additionalHeaders.forEach(header::appendField);
        }

        return JWSHeader.parse(header);
    }
}
