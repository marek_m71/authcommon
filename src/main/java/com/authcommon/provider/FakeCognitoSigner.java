package com.authcommon.provider;

import com.authcommon.exception.JWTProviderException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.UUID;

/**
 * This class/bean allows us to create our own Cognito tokens, signed by ourselves.
 * It stores relevant information for use later on when testing, or when needed by the JWTFactory etc
 */
@Service
public class FakeCognitoSigner {

    private static final Logger LOG = LoggerFactory.getLogger(FakeCognitoSigner.class);
    private static final String RSA_ALGORITHM = "RSA";
    private static final int KEYSIZE_2048 = 2048;

    private final KeyPair keyPair;
    private final JWK jwk;
    private final String keyId;

    public FakeCognitoSigner() {
        try {
            KeyPairGenerator gen = KeyPairGenerator.getInstance(RSA_ALGORITHM);
            gen.initialize(KEYSIZE_2048);
            keyPair = gen.generateKeyPair();
            keyId = UUID.randomUUID().toString();

            jwk = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
                    .privateKey((RSAPrivateKey) keyPair.getPrivate())
                    .keyUse(KeyUse.SIGNATURE)
                    .keyID(keyId)
                    .build();

        } catch (Exception e) {
            LOG.error("Couldn't create FakeCognitoSigner as something went wrong", e);
            throw new JWTProviderException("Couldn't create FakeCognitoSigner as something went wrong", e);
        }
    }

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public JWK getJwk() {
        return jwk;
    }

    public String getKeyId() {
        return keyId;
    }
}
