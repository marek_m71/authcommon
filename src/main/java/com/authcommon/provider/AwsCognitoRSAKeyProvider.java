package com.authcommon.provider;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.JwkProviderBuilder;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import com.authcommon.exception.JWTProviderException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * This is the key provider for JWTs,
 * it sets up so it can retrieve from the Cognito well-known URL the JWKS.
 * <p>
 * The JWKS data is automatically cached for ~10hrs by default.
 */
@Service
public class AwsCognitoRSAKeyProvider implements RSAKeyProvider {

    private final URL awsWellKnownURL;

    public AwsCognitoRSAKeyProvider(@Value("${jwt.cognito.issuer}") final String issuer) {
        String url = String.format("%s/.well-known/jwks.json", issuer);
        try {
            this.awsWellKnownURL = new URL(url);
        } catch (MalformedURLException e) {
            throw new JWTProviderException(String.format("Invalid URL provided, URL=%s", url), e);
        }
    }

    @Override
    public RSAPublicKey getPublicKeyById(String kid) {
        try {
            JwkProvider provider = new JwkProviderBuilder(awsWellKnownURL).cached(true).build();
            Jwk jwk = provider.get(kid);
            return (RSAPublicKey) jwk.getPublicKey();
        } catch (Exception e) {
            throw new JWTProviderException(String.format("Failed to get JWT kid=%s from awsWellKnownURL=%s", kid, awsWellKnownURL), e);
        }
    }

    @Override
    public RSAPrivateKey getPrivateKey() {
        return null;
    }

    @Override
    public String getPrivateKeyId() {
        return null;
    }
}
