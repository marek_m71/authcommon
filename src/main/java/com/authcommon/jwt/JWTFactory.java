package com.authcommon.jwt;

import java.util.Map;

public interface JWTFactory {
    String generateUserToken(String accountId);

    String generateServiceToken() throws Exception;

    String generateToken(Map<String, String> claimMap);

    String generateToken(Map<String, String> claimMap, int tokenTTLMinutes);
}
