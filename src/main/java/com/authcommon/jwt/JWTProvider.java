package com.authcommon.jwt;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.authcommon.exception.BadRequestException;
import com.authcommon.exception.InvalidTokenException;
import com.authcommon.exception.JWTProviderException;
import com.nimbusds.jwt.SignedJWT;
import com.authcommon.constant.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;


/**
 * Utility for actions around JWT's, including validation
 */
public class JWTProvider {

    private static final Logger LOG = LoggerFactory.getLogger(JWTProvider.class);

    private final JWTVerifier verifier;

    public JWTProvider(final JWTVerifier verifier) {
        this.verifier = verifier;
    }

    /**
     * Get's a given string claim from a JWT
     *
     * @param jwt       the jwt
     * @param claimName the name of the claim to get
     * @return the value of the claim
     * @throws JWTProviderException if cannot parse the token for claims
     * @should return claim if token is valid and claims exists
     * @should throw exception if jwt is null
     * @should throw exception if jwt is empty
     * @should throw exception if unable to parse jwt
     */
    public String getStringClaim(final String jwt, final String claimName) throws JWTProviderException {
        try {
            assertNotBlank(jwt);
            return SignedJWT.parse(jwt).getPayload().toJSONObject().getAsString(claimName);
        } catch (ParseException e) {
            LOG.error("Unable to parse JWT and thus unable to retrieve claim {}", claimName, e);
            throw new JWTProviderException(Constants.FAILED_TO_PARSE_JWT_TOKEN_ERROR_MESSAGE, e);
        }
    }

    /**
     * Verifies the given JWT against the shared key.
     *
     * @param jwt the JWT token
     * @return true if the given JWT token is valid
     * @should pass without error if valid
     * @should throw exception if unable to verify jwt
     * @should throw InvalidTokenException if token has expired
     * @should throw exception if jwt is empty string
     * @should throw exception if jwt is null
     * @should throw exception if unable to parse jwt
     */
    public void verify(final String jwt) {
        try {
            assertNotBlank(jwt);
            SignedJWT signedJWT = SignedJWT.parse(jwt);
            if (tokenHasExpired(signedJWT)) {
                throw new InvalidTokenException(Constants.JWT_EXPIRED_ERROR_MESSAGE);
            }
            verifier.verify(jwt);
        } catch (ParseException e) {
            LOG.error(Constants.FAILED_TO_PARSE_CLAIMS_ERROR_MESSAGE);
            throw new JWTProviderException(Constants.FAILED_TO_PARSE_CLAIMS_ERROR_MESSAGE, e);
        } catch (JWTVerificationException e) {
            throw new InvalidTokenException(Constants.INVALID_TOKEN_ERROR_MESSAGE);
        }
    }

    /**
     * Verifies if the given token has expired
     *
     * @param signedJWT the given token
     * @return true if the token has expired
     * @throws ParseException failure to parse the token claims
     */
    private boolean tokenHasExpired(final SignedJWT signedJWT) throws ParseException {
        Date rootExpirationTime = signedJWT.getJWTClaimsSet().getExpirationTime();
        Date newExpirationTime = subtractExpirationTime(rootExpirationTime);

        return new Date().after(rootExpirationTime);
    }

    private Date subtractExpirationTime(Date rootExpirationTime) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(rootExpirationTime);
        cal.add(Calendar.MINUTE, -54);
        return cal.getTime();
    }

    /**
     * Asserts the given JWT token not blank
     *
     * @param jwt the given JWT token
     */
    private void assertNotBlank(String jwt) {
        if (StringUtils.isEmpty(jwt)) {
            throw new BadRequestException(Constants.NULL_OR_EMPTY_JWT_ERROR_MESSAGE);
        }
    }
}
