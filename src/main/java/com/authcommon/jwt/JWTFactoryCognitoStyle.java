package com.authcommon.jwt;

import com.authcommon.constant.Constants;
import com.authcommon.exception.JWTProviderException;
import com.authcommon.provider.FakeCognitoSigner;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.apache.http.MethodNotSupportedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility for creating JWT's
 */
public class JWTFactoryCognitoStyle implements JWTFactory {

    private static final Logger LOG = LoggerFactory.getLogger(JWTProvider.class);

    private final String issuer;
    private final int defaultTokenTTLMinutes;
    private final FakeCognitoSigner fakeCognitoSigner;

    public JWTFactoryCognitoStyle(final FakeCognitoSigner fakeCognitoSigner,
                                  final String issuer,
                                  final int defaultTokenTTLMinutes) {
        this.defaultTokenTTLMinutes = defaultTokenTTLMinutes;
        this.issuer = issuer;
        this.fakeCognitoSigner = fakeCognitoSigner;
    }

    /**
     * Generates a JWT token in the context of a user
     *
     * @param accountId the user account id
     * @return JWT token with type APPLICATION_USER, containing the user account id
     * @should add all claims and create Asto User JWT
     * @should throw exception if signing fails
     */
    @Override
    public String generateUserToken(final String accountId) {
        Map<String, String> claims = new HashMap<>();
        claims.put(Constants.ACCOUNT_ID_COGNITO_CLAIM, accountId);
        return generateToken(claims);
    }

    /**
     * Generates a JWT token in the context of a service
     *
     * @return JWT token with type APPLICATION
     * @should add all claims and create JWT
     * @should throw exception if signing fails
     */
    @Override
    public String generateServiceToken() throws Exception {
        throw new MethodNotSupportedException("This method is not supported for Cognito style tokens");
    }

    /**
     * Generate a signed JWT with given claims and the default token TTL
     *
     * @param claimMap the claims to add to the JWT
     * @return JWT token
     * @should add all claims and create JWT
     * @should throw exception if signing fails
     */
    @Override
    public String generateToken(final Map<String, String> claimMap) {
        return generateToken(claimMap, defaultTokenTTLMinutes);
    }

    /**
     * Generate a signed JWT with the given claims and token TTL
     *
     * @param claimMap        the claims to add to the JWT
     * @param tokenTTLMinutes the TTL in minutes that the token will be valid for
     * @return JWT token
     * @should add claims and create JWT
     * @should throw exception when signing fails
     */
    @Override
    public String generateToken(final Map<String, String> claimMap, final int tokenTTLMinutes) {
        try {
            JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder()
                    .issuer(issuer)
                    .expirationTime(convertToDateViaInstant(LocalDateTime.now().plusMinutes(tokenTTLMinutes)));

            claimMap.forEach(builder::claim);

            SignedJWT signedJWT = new SignedJWT(
                    new JWSHeader.Builder(JWSAlgorithm.RS256)
                            .keyID(fakeCognitoSigner.getKeyId())
                            .build(),
                    builder.build());

            signedJWT.sign(new RSASSASigner(fakeCognitoSigner.getKeyPair().getPrivate()));
            return signedJWT.serialize();
        } catch (JOSEException e) {
            LOG.error("Failed to sign the JWT", e);
            throw new JWTProviderException("Failed to sign the JWT", e);
        }
    }

    /**
     * Converts to {@link Date}
     *
     * @param dateToConvert the given LocalDateTime
     * @return {@link Date}
     */
    private Date convertToDateViaInstant(final LocalDateTime dateToConvert) {
        return Date.from(dateToConvert.atZone(ZoneId.systemDefault())
                .toInstant());
    }
}
