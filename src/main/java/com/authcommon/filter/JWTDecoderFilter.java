package com.authcommon.filter;

import com.authcommon.constant.Constants;
import com.authcommon.domain.Membership;
import com.authcommon.exception.InvalidTokenException;
import com.authcommon.exception.JWSException;
import com.authcommon.exception.JWTFilterException;
import com.authcommon.exception.JWTProviderException;
import com.authcommon.jws.JWSUtils;
import com.authcommon.jwt.JWTProvider;
import com.authcommon.token.MembershipToken;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * JWT Decoder Filter use to verify all JWT tokens (JWT default token and Cognito)
 */
public class JWTDecoderFilter extends GenericFilterBean {

    private static final Logger LOG = LoggerFactory.getLogger(JWTDecoderFilter.class);
    private static final String USERNAME_COGNITO_CLAIM_ACCESS_TOKEN = "username";
    private static final String USERNAME_COGNITO_CLAIM_ID_TOKEN = "cognito:username";
    private static final String USERNAME = "username";

    private final Map<String, JWTProvider> providerMap;
    private final JWSUtils jwsUtils;
    private final String cognitoJwtIssuer;
    private final String jwtIssuer;

    public JWTDecoderFilter(
            @Qualifier("cognito-jwt-provider") final JWTProvider cognitoJwtProvider,
            @Qualifier("jwt-provider") final JWTProvider jwtProvider,
            @Value("${jwt.cognito.issuer}") final String cognitoJwtIssuer,
            @Value("${jwt.issuer}") final String jwtIssuer,
            final JWSUtils jwsUtils) {

        this.cognitoJwtIssuer = cognitoJwtIssuer;
        this.jwtIssuer = jwtIssuer;
        providerMap = new HashMap<>();
        providerMap.put(cognitoJwtIssuer, cognitoJwtProvider);
        providerMap.put(jwtIssuer, jwtProvider);
        this.jwsUtils = jwsUtils;
    }

    /**
     * Filters incoming requests to decode the JWT and create the relevant security context.
     *
     * @param request  the request
     * @param response the response
     * @param chain    the filter chain
     * @should successfully filter if valid default service jwt
     * @should successfully filter if valid cognito user jwt
     * @should successfully filter if path start with /no-auth
     * @should return unauthorized if jwt is null
     * @should return unauthorized if jwt is empty
     * @should return unauthorized if issuer is null
     * @should return unauthorized if issuer is empty
     * @should return unauthorized if account id is null
     * @should return unauthorized if account id is empty
     * @should return unauthorized if provider does not exist for issuer
     * @should return unauthorized if user type is not one of known types
     * @should return unauthorized if jwsUtils throws JWSException
     * @should return unauthorized if jwtProvider throws JWTProviderException
     * @should return unauthorized if jwtProvider throws InvalidTokenException
     */
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String jwt = httpRequest.getHeader(Constants.AUTHORIZATION_HEADER_NAME);
        try {
            if (!(StringUtils.isNotBlank(httpRequest.getServletPath()) && httpRequest.getServletPath().startsWith("/no-auth"))) {
                if (StringUtils.isNotBlank(jwt)) {
                    JWTProvider jwtProvider = getJWTProvider(jwt);
                    jwtProvider.verify(jwt);
                    setUpSecurityContext(jwtProvider, jwt, httpRequest);
                } else {
                    LOG.error("No Authorization header on request");
                    throw new JWTFilterException("No Authorization header on request");
                }
            }

            chain.doFilter(request, response);
        } catch (final JWSException | JWTFilterException | JWTProviderException | InvalidTokenException ex) {
            HttpServletResponse resp = (HttpServletResponse) response;
            resp.reset();
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    /**
     * Sets up the security context based on the token claims
     *
     * @param jwtProvider the jwt provider for the token
     * @param jwt         the token
     * @param httpRequest the original request, to derive org-id header
     */
    private void setUpSecurityContext(final JWTProvider jwtProvider,
                                      final String jwt,
                                      final HttpServletRequest httpRequest) {
        String iss = jwtProvider.getStringClaim(jwt, Constants.ISSUER_CLAIM);
        if (jwtIssuer.equals(iss)) {
            String username = jwtProvider.getStringClaim(jwt, USERNAME);
            if (StringUtils.isNotBlank(username)) {
                SecurityContextHolder.getContext().setAuthentication(new MembershipToken(new Membership(username, jwt)));
            } else {
                throw new JWTFilterException("Username is not present in the JWT");
            }
        } else {
            String issuer = jwtProvider.getStringClaim(jwt, Constants.ISSUER_CLAIM);
            if (cognitoJwtIssuer.equals(issuer)) {
                String username = jwtProvider.getStringClaim(jwt, USERNAME_COGNITO_CLAIM_ACCESS_TOKEN);
                if (StringUtils.isBlank(username)) {
                    username = jwtProvider.getStringClaim(jwt, USERNAME_COGNITO_CLAIM_ID_TOKEN);
                }
                if (StringUtils.isNotBlank(username)) {
                    SecurityContextHolder.getContext().setAuthentication(new MembershipToken(new Membership(username, jwt)));
                } else {
                    throw new JWTFilterException("Username is not present in the JWT");
                }
            } else {
                throw new JWTFilterException("Failed to verify the JWT token, not a recognised supported type to parse username");
            }
        }
    }

    /**
     * Gets the correct JWT provider for the token, based on the issuer
     *
     * @param jwt the token
     * @return a valid provider for the issuer declared in the token
     * @throws JWSException       if cannot parse the JWT
     * @throws JWTFilterException if the issuer is null or no provider can be derived for the issuer
     */
    private JWTProvider getJWTProvider(final String jwt) throws JWSException, JWTFilterException {
        String issuer = jwsUtils.getStringClaim(jwt, Constants.ISSUER_CLAIM);
        if (StringUtils.isBlank(issuer)) {
            throw new JWTFilterException("No issuer claim found in JWT, cannot verify");
        }

        JWTProvider jwtProvider = providerMap.get(issuer);

        if (jwtProvider == null) {
            throw new JWTFilterException(String.format("No jwt provider found for issuer %s", issuer));
        }

        return jwtProvider;
    }
}
