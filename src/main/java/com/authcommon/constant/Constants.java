package com.authcommon.constant;

/**
 * Class holding all commons constant in relation to JWT
 */
public class Constants {

    public static final String JWT_EXPIRED_ERROR_MESSAGE = "JWT has expired";
    public static final String FAILED_TO_PARSE_CLAIMS_ERROR_MESSAGE = "Failed to parse JWT claims";
    public static final String NULL_OR_EMPTY_JWT_ERROR_MESSAGE = "JWT token must not be null or empty";
    public static final String INVALID_TOKEN_ERROR_MESSAGE = "Invalid JWT token";
    public static final String FAILED_TO_PARSE_JWT_TOKEN_ERROR_MESSAGE = "Unable to parse jwt";
    public static final String ISSUER_CLAIM = "iss";
    public static final String ACCOUNT_ID_COGNITO_CLAIM = "username";
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private Constants() {
        // should not be instantiated
    }
}
