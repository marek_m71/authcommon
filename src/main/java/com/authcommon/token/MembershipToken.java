package com.authcommon.token;

import com.authcommon.domain.Membership;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;
import java.util.Optional;

/**
 * Holds the {@link Membership}  so that it can be added to a {@link SecurityContextHolder}
 * for reference elsewhere in the application
 */
public class MembershipToken extends AbstractAuthenticationToken {

    private final Membership membership;

    public MembershipToken(final Membership membership) {
        super(Collections.emptyList());
        this.membership = membership;
    }

    /**
     * @should return empty optional
     */
    @Override
    public Object getCredentials() {
        return Optional.empty();
    }

    /**
     * @should return user id
     */
    @Override
    public Object getPrincipal() {
        return membership;
    }
}
