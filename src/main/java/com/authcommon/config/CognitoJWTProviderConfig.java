package com.authcommon.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.authcommon.jwt.JWTFactory;
import com.authcommon.jwt.JWTFactoryCognitoStyle;
import com.authcommon.jwt.JWTProvider;
import com.authcommon.provider.AwsCognitoRSAKeyProvider;
import com.authcommon.provider.FakeCognitoSigner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * Creates a {@link JWTProvider} for WSO2 tokens
 */
@Configuration
public class CognitoJWTProviderConfig extends JWTProviderConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(CognitoJWTProviderConfig.class);

    private final String issuer;
    private final AwsCognitoRSAKeyProvider awsCognitoRSAKeyProvider;
    private final FakeCognitoSigner fakeCognitoSigner;

    public CognitoJWTProviderConfig(
            @Value("${jwt.cognito.issuer}") final String issuer,
            AwsCognitoRSAKeyProvider awsCognitoRSAKeyProvider,
            FakeCognitoSigner fakeCognitoSigner) {
        super(null, issuer);
        this.issuer = issuer;
        this.awsCognitoRSAKeyProvider = awsCognitoRSAKeyProvider;
        this.fakeCognitoSigner = fakeCognitoSigner;
    }

    /**
     * Creates a cognito jwt provider
     *
     * @return a cognito jwt provider
     * @should correctly create an instance
     */
    @Bean("cognito-jwt-provider")
    @Primary
    public JWTProvider getJWTProvider() {

        Algorithm algorithm = Algorithm.RSA256(awsCognitoRSAKeyProvider);
        return new JWTProvider(JWT.require(algorithm)
                .acceptLeeway(ACCEPTED_LEEWAY_IN_SECONDS)
                .withIssuer(issuer)
                .build());
    }

    @Bean("cognito-jwt-factory")
    @Primary
    public JWTFactory getJWTFactory() {
        try {
            return new JWTFactoryCognitoStyle(fakeCognitoSigner, issuer, 60);
        } catch (Exception e) {
            LOG.error("Couldn't do a thing", e);
            return null;
        }
    }
}
