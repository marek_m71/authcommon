package com.authcommon.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.authcommon.jwt.JWTProvider;
import org.apache.commons.codec.binary.Base64;

/**
 * Base class for creating a {@link JWTProvider}
 */
public abstract class JWTProviderConfiguration {

    protected static final long ACCEPTED_LEEWAY_IN_SECONDS = 60;

    private final String secret;
    private final String issuer;

    protected JWTProviderConfiguration(final String secret,
                                       final String issuer) {
        this.secret = secret;
        this.issuer = issuer;
    }

    protected JWTProvider getProvider() {
        return new JWTProvider(getVerifier());
    }

    /**
     * Creates an instance of {@link JWTVerifier} for the given secret
     *
     * @return an instance of {@link JWTVerifier}
     */
    private JWTVerifier getVerifier() {
        return JWT.require(Algorithm.HMAC256(Base64.decodeBase64(secret)))
                .acceptLeeway(ACCEPTED_LEEWAY_IN_SECONDS)
                .withIssuer(issuer)
                .build();
    }
}
