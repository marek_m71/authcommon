package com.authcommon.config;

import com.authcommon.jwt.JWTProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JWTProviderConfig extends JWTProviderConfiguration {
    @Autowired
    public JWTProviderConfig(@Value("${jwt.secret}") String secret, @Value("${jwt.issuer}") String issuer) {
        super(secret, issuer);
    }

    @Bean({"jwt-provider"})
    public JWTProvider getJWTProvider() {
        return this.getProvider();
    }
}
