package com.authcommon.domain;

import org.apache.commons.lang3.RandomStringUtils;

public class ServiceMembership extends Membership {

    public static final String SERVICE_ID = RandomStringUtils.randomAlphanumeric(16);

    public ServiceMembership() {
        super(SERVICE_ID, null);
    }

}
