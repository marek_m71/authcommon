package com.authcommon.domain;

import java.util.Objects;

/**
 * Represents a membership in services
 */
public class Membership {
    private final String id;
    private final String token;

    public Membership(String id, String token) {
        this.id = id;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Membership that = (Membership) o;

        if (!Objects.equals(id, that.id)) return false;
        return (Objects.equals(token, that.token));
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result;
        return result;
    }

    @Override
    public String toString() {
        return "Membership{" +
                "id='" + id + '\'' +
                '}';
    }
}
